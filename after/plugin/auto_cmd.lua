-- wrap text in these formatted files
local wrap_group = vim.api.nvim_create_augroup('TextWrap', { clear = true })
vim.api.nvim_create_autocmd('BufEnter', {
    pattern = { '*.md', '*.mdx', '*.txt' },
    group = wrap_group,
    command = 'setlocal wrap',
})

local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
    callback = function()
        vim.highlight.on_yank()
    end,
    group = highlight_group,
    pattern = '*',
})
