vim.api.nvim_create_user_command(
    'CloseBufferButMaintainSplit',
    function()
        local success, _ = pcall(vim.cmd, "bp|bd #") -- close buffer without closing split
        if not success then
            local ok, _ = pcall(vim.cmd, "close")    -- if same buffer in both splits close one of the splits
            if not ok then
                vim.cmd [[bd]]
            end
        end
    end,
    { nargs = 0 }
)

vim.api.nvim_create_user_command(
    'Config',
    function(_)
        local path = "~/.config/nvim"
        vim.cmd(string.format("Neotree current %s", path, path))
        find_related_files(false)
    end,
    { nargs = 0 }
)


vim.api.nvim_create_user_command(
    'FuzzyFindInFile',
    function(_)
        -- You can pass additional configuration to telescope to change theme, layout, etc.
        require('telescope.builtin').current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
            winblend = 10,
            previewer = true,
        })
    end,
    { nargs = 0 }
)


-- Create a command `:Format` local to the LSP buffer
vim.api.nvim_create_user_command('FormatBuf', function()
    if vim.bo.filetype == 'py' then
        local file_name = vim.fn.expand('%:p')
        vim.fn.system('black' .. vim.fn.shellescape(file_name))
    else
        vim.lsp.buf.format()
    end
end, { desc = 'Format current buffer with LSP' })
