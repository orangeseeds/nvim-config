local function map(mode, lhs, rhs, opts)
    local options = { silent = true }
    if opts then
        options = vim.tbl_extend("force", options, opts)
    end
    vim.keymap.set(mode, lhs, rhs, options)
end

local harpoon = require("harpoon")
local t_builtin = require('telescope.builtin')
local telescope_utils = require('telescope.utils')
local telescope = require('telescope')

---------------------------------------------------------
-- Leader Key
---------------------------------------------------------
map({ 'n', 'v' }, '<Space>', '<Nop>')

---------------------------------------------------------
-- Disable Arrow Keys
---------------------------------------------------------
map({ 'n', 'i' }, '<Up>', '<nop>');
map({ 'n', 'i' }, '<Down>', '<nop>');
map({ 'n', 'i' }, '<Left>', '<nop>');
map({ 'n', 'i' }, '<Right>', '<nop>');

---------------------------------------------------------
-- Redo
---------------------------------------------------------
map({ "n" }, "U", "<C-r>", { desc = "redo" })

---------------------------------------------------------
-- Buffer Actions
---------------------------------------------------------
map({ "n" }, "<leader>v", ":vsplit<CR>", { desc = "split vertically" })
map({ "n" }, "<leader>h", ":split<CR>", { desc = "split horizontally" })

map('n', '<Right>', ':bnext<CR>', { desc = "next buffer" })
map('n', '<Left>', ':bprevious<CR>', { desc = "previous buffer" })
map('', '<C-x>', '<Nop>')
map('', '<C-x>', ':CloseBufferButMaintainSplit<CR>', { desc = "close buffer without closing split" })
map("", '<C-d>', ':close<CR>', { desc = "close split without closing buffer" })

---------------------------------------------------------
-- Resize Actions
---------------------------------------------------------
map('n', '<C-Up>', ':resize +2<CR>')
map('n', '<C-Down>', ':resize -2<CR>')
map('n', '<C-Left>', ':vertical resize -2<CR>')
map('n', '<C-Right>', ':vertical resize +2<CR>')

---------------------------------------------------------
-- Select Actions
---------------------------------------------------------
map('n', 'x', 'V')
map('n', 'X', 'V')
map('v', 'x', 'j')
map('v', 'X', 'k')

---------------------------------------------------------
-- File Browsing Actions
---------------------------------------------------------
map('n', 'ge', '$')
map('n', 'gs', '0')

---------------------------------------------------------
-- Clipboard Actions
---------------------------------------------------------
map("n", "<leader>p", '"0p')

---------------------------------------------------------
-- Harpoon Actions
---------------------------------------------------------

map("n", "<leader>a", function()
    harpoon:list():add()
    print("buffer added to harpoon list!")
end)
map("n", "<leader>e", function() harpoon.ui:toggle_quick_menu(harpoon:list()) end)

map({ "n" }, "<C-p>", function() harpoon:list():prev() end)
map({ "n" }, "<C-n>", function() harpoon:list():next() end)

---------------------------------------------------------
-- LSP Actions
---------------------------------------------------------
map('n', '[d', vim.diagnostic.goto_prev, { desc = "Go to previous diagnostic message" })
map('n', ']d', vim.diagnostic.goto_next, { desc = "Go to next diagnostic message" })
map('n', '<leader>d', vim.diagnostic.open_float, { desc = "Open floating diagnostic message" })


map('n', '<leader>rn', vim.lsp.buf.rename, { desc = '[R]e[n]ame' })
map('n', '<leader>ca', vim.lsp.buf.code_action, { desc = '[C]ode [A]ction' })

map('n', 'gd', vim.lsp.buf.definition, { desc = '[G]oto [D]efinition' })
map('n', 'gr', t_builtin.lsp_references, { desc = '[G]oto [R]eferences' })
map('n', 'gI', vim.lsp.buf.implementation, { desc = '[G]oto [I]mplementation' })
map('n', '<leader>D', vim.lsp.buf.type_definition, { desc = 'Type [D]efinition' })
map('n', '<leader>ds', t_builtin.lsp_document_symbols, { desc = '[D]ocument [S]ymbols' })
map('n', '<leader>ws', t_builtin.lsp_dynamic_workspace_symbols, { desc = '[W]orkspace [S]ymbols' })

map('n', '<leader>k', vim.lsp.buf.hover, { desc = 'Hover Documentation' })
map('n', '<C-s>', vim.lsp.buf.signature_help, { desc = 'Signature Documentation' })

map('n', 'gD', vim.lsp.buf.declaration, { desc = '[G]oto [D]eclaration' })
-- map('n', '<leader>wa', vim.lsp.buf.add_workspace_folder, { desc = '[W]orkspace [A]dd Folder' })
-- map('n', '<leader>wr', vim.lsp.buf.remove_workspace_folder, { desc = '[W]orkspace [R]emove Folder' })
-- map('n', '<leader>wl', function()
--     print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
-- end, { desc = '[W]orkspace [L]ist Folders' })

---------------------------------------------------------
-- Delimiter Actions
---------------------------------------------------------
map({ "i", "n" }, ";;", "<Esc>A;<Esc>", { desc = "Put a ; at the end of the line" })

---------------------------------------------------------
-- Telescope Actions
---------------------------------------------------------
map('n', '<leader>fr', t_builtin.oldfiles, { desc = '[?] Find recently opened files' })
map('n', '<leader>fb', t_builtin.buffers, { desc = '[ ] Find existing buffers' })
map('n', '<leader>/', ":FuzzyFindInFile<CR>", { desc = '[/] Fuzzily search in current buffer' })
map('n', '<leader>ff', function() find_related_files(false) end, { desc = '[F]ind [f]iles' })
map('n', '<leader>fF', function()
    t_builtin.find_files({ cwd = telescope_utils.buffer_dir() })
end, { desc = '[F]earch [F]iles' })
map('n', '<leader>fh', t_builtin.help_tags, { desc = '[S]earch [H]elp' })
map('n', '<leader>fw', t_builtin.grep_string, { desc = '[S]earch current [W]ord' })
map('n', '<leader>fg', function() grep_related_files(false) end, { desc = '[S]earch by [G]rep' })
map('n', '<leader>fd', t_builtin.diagnostics, { desc = '[S]earch [D]iagnostics' })

map('n', '<leader>gf', t_builtin.git_files, { desc = 'Search [G]it [F]iles' })
map('n', '<leader>gw', telescope.extensions.git_worktree.git_worktrees,
    { desc = 'Search [G]it [W]orktree' })
map('n', '<leader>gc', telescope.extensions.git_worktree.create_git_worktree,
    { desc = 'Search [G]it [C]reate worktree' })

map('n', '<leader>.', t_builtin.resume, { desc = "resume previous search" })


---------------------------------------------------------
-- NeoTree Actions
---------------------------------------------------------
map('n', '<leader>L', function()
    local buffer_directory = vim.fn.expand('%:p:h')
    local command = string.format("Neotree %s", buffer_directory)
    vim.cmd(command)
end, { desc = 'Show cwd' })

map('n', '<leader>l', ":Neotree<CR>", { desc = "Show cwd project" })

---------------------------------------------------------
-- Quickfix Lists
---------------------------------------------------------
map('n', '<C-j>', ":cprev<CR>", { desc = 'goto previous Quickfix' })
map('n', '<C-k>', ":cnext<CR>", { desc = 'goto next Quickfix' })

---------------------------------------------------------
-- Todo Actions
---------------------------------------------------------
map("n", "<leader>o", ':TodoTelescope<CR>', { silent = true })

---------------------------------------------------------
-- Toggle Linenumber
---------------------------------------------------------
map('', '<leader>uh', "<Esc>:setlocal number! relativenumber!<CR>")


-- Copilot Actions
-- vim.g.copilot_no_tab_map = true
-- vim.g.copilot_filetypes = {
--     ["*"] = false,
-- ["javascript"] = true,
-- ["typescript"] = true,
-- ["svelte"] = true,
-- ["lua"] = false,
-- ["rust"] = true,
-- ["c"] = true,
-- ["c#"] = true,
-- ["c++"] = true,
-- ["go"] = true,
-- ["python"] = true,
-- }

-- keymap("i", "<leader><Tab>", 'copilot#Accept("<CR>")', { silent = true, expr = true })
