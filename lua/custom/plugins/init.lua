-- You can add your own plugins here or in other files in this directory!
--  I promise not to create any merge conflicts in this directory :)
--
-- See the kickstart.nvim README for more information
-- return {
--   'echasnovski/mini.nvim',
--   version = '*',
--   config = function()
--     local header = [[
--   /\ \▔\___  ___/\   /(●)_ __ ___
--  /  \/ / _ \/ _ \ \ / / | '_ ` _ \
-- / /\  /  __/ (_) \ V /| | | | | | |
-- \_\ \/ \___|\___/ \_/ |_|_| |_| |_|
-- ───────────────────────────────────]]
--
--     local starter = require('mini.starter')
--     starter.setup({
--       header = header,
--       -- evaluate_single = true,
--       items = {
--         starter.sections.recent_files(8, false),
--       },
--       content_hooks = {
--         starter.gen_hook.adding_bullet(),
--         starter.gen_hook.padding(3, 2),
--       },
--     })
--   end
-- }


return {

}
