local colors150 = {
    "luckydev/150colors"
}

local papercolor = {
    "NLKNguyen/papercolor-theme"
}

local catpuccin = {
    "catppuccin/nvim",
    name = "catppuccin",
    priority = 1000,
}

local gruvbox = {
    "ellisonleao/gruvbox.nvim",
    priority = 1000,


    config = function()
    end,
}

local tokyonight = {
    "folke/tokyonight.nvim",
    lazy = false,
    priority = 1000,
    opts = {},
}

local kanagawa = {
    "rebelot/kanagawa.nvim",

    -- config = function()
    --     vim.cmd.colorscheme 'kanagawa-dragon'
    -- end,
}

local brown = {
    "bhrown/brown.vim",

    -- config = function()
    --     vim.cmd.colorscheme 'brown'
    -- end,
}

local gruvboxmaterial = {
    "sainnhe/gruvbox-material",

    -- config = function()
    --     vim.cmd.colorscheme 'gruvbox-material'
    -- end,
}

local miasma = {
    "xero/miasma.nvim",
    lazy = false,
    priority = 1000,

    -- config = function()
    --   vim.cmd.colorscheme = 'miasma'
    -- end,
}

local everforest = {
    "sainnhe/everforest",

    -- config = function()
    --   vim.cmd.colorscheme = 'everforest'
    -- end,
}

local bamboo = {
    'ribru17/bamboo.nvim',
    lazy = false,
    priority = 1000,

    -- config = function()
    --     require('bamboo').setup {
    --         -- optional configuration here
    --     }
    --   vim.cmd.colorscheme = 'bamboo'
    -- end,
}

local gruvvsassist = {
    "bartekprtc/gruv-vsassist.nvim",

}

local onedark = {
    "joshdick/onedark.vim",

    -- config = function()
    --   vim.cmd.colorscheme = 'onedark'
    -- end,
}

local srcery = {
    "srcery-colors/srcery-vim",
    -- config = function()
    --   vim.cmd.colorscheme = 'srcery'
    -- end,
}

local gruvbit = {
    "habamax/vim-gruvbit"

}
local caret = {
    "projekt0n/caret.nvim"
}

local subatomic = {
    "d11wtq/subatomic256.vim"
}

local pack = {
    'daviddavis/vim-colorpack'
}

local colorschemes = {
    gruvbit,
    gruvbox,
    -- onedark,
    -- caret,
    -- tokyonight,
    -- bamboo,
    -- srcery,
    -- catpuccin,
    -- kanagawa,
    -- brown,
    -- gruvboxmaterial,
    -- miasma,
    -- subatomic,
    -- gruvvsassist,
    -- everforest,
    -- papercolor,
    -- pack,
}

return colorschemes
