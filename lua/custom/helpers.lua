-- helper function to check if a directory is a project root directory
---@diagnostic disable-next-line: lowercase-global
function is_project_root(dir)
    local project_files = {
        ".git",
        ".hg",
        "Cargo.toml",
        "go.mod",
        "package.json",
        "composer.json",
        "requirements.txt",
        "Dockerfile",

    }
    for _, file in ipairs(project_files) do
        if vim.fn.isdirectory(dir .. '/' .. file) == 1 then
            return true
        end
    end
    return false
end

-- helper function to find the project root directory
---@diagnostic disable-next-line: lowercase-global
function find_project_root(dir)
    if is_project_root(dir) then
        return dir
    end

    -- if not, traverse up the directory hierarchy until a project root directory is found
    local parent_dir = vim.fn.fnamemodify(dir, ':h')
    if parent_dir == dir then
        return nil
    end
    return find_project_root(parent_dir)
end

local function get_telescope_opts(title, root, display_hidden)
    local opts = {
        no_ignore = display_hidden,
        hidden = display_hidden,
        prompt_title = title,
        cwd = root,
        attach_mappings = function(_, map)
            -- Use <CR> to select a file
            map('i', '<CR>', require("telescope.actions").select_default)
            -- Use <C-x> to delete a buffer
            map('i', '<C-x>', function(prompt_bufnr)
                require("telescope.actions").delete_buffer(prompt_bufnr)
                require("telescope.actions").close(prompt_bufnr)
            end)
            return true
        end
    }
    return opts
end

function grep_related_files(display_hidden)
    local project_root = find_project_root(vim.fn.expand('%:p:h'))
    if project_root == nil then
        -- print("Project root not found!")
        local opts = { noremap = true, silent = true }
        require("telescope.builtin").live_grep(opts)
        return
    end

    local opts = get_telescope_opts("Grep Related Files ", project_root, display_hidden)
    require("telescope.builtin").live_grep(opts)
end

-- find files related to project directory of the currently open buffer
---@diagnostic disable-next-line: lowercase-global
function find_related_files(display_hidden)
    local project_root = find_project_root(vim.fn.expand('%:p:h'))
    if project_root == nil then
        -- print("Project root not found!")
        local opts = {
            noremap = true,
            silent = true,
        }
        require("telescope.builtin").find_files(opts)
        return
    end

    local opts = get_telescope_opts("Find Files", project_root, display_hidden)
    require("telescope.builtin").find_files(opts)
end

---@diagnostic disable-next-line: lowercase-global
function wrap_text(str, max_width)
    local chunks = {}
    for i = 1, #str, max_width do
        table.insert(chunks, string.sub(str, i, i + max_width - 1))
    end
    return table.concat(chunks, "\n")
end

---@diagnostic disable-next-line: lowercase-global
function print_and_erase(message, delay)
    local line = vim.api.nvim_get_current_line()
    vim.api.nvim_echo({ { message, "Normal" } }, true, {})
    vim.defer_fn(function()
        vim.cmd(":echo ''")
    end, delay * 1000)
end

---@diagnostic disable-next-line: lowercase-global
function get_projects()
    local project_nvim = require("project_nvim")
    local recent_projects = project_nvim.get_recent_projects()

    local projects = {}

    for _, client in ipairs(recent_projects) do
        local substrings = {}
        for substring in client:gmatch("[^/]+") do
            table.insert(substrings, substring)
        end
        local dir_name = substrings[#substrings]
        local short_path = { unpack(substrings, math.min(3, #substrings), #substrings) }
        projects[#projects + 1] = {
            name = string.format("%s (%s)", dir_name, table.concat(short_path, "/")),
            action = ":cd " .. client .. " | e ./" .. "| Telescope find_files",
            section = "Projects"
        }
    end

    local reverse = {}
    for i = #projects, math.min(1, #projects - 8), -1 do
        reverse[#reverse + 1] = projects[i]
    end
    return reverse
end

-- Trash the target
function trash(state)
    local inputs = require("neo-tree.ui.inputs")
    local path = state.tree:get_node().path
    local msg = "Are you sure you want to trash " .. path
    inputs.confirm(msg, function(confirmed)
        if not confirmed then return end

        vim.fn.system { "trash", vim.fn.fnameescape(path) }
        require("neo-tree.sources.manager").refresh(state.name)
    end)
end

-- Trash the selections (visual mode)
function trash_visual(state, selected_nodes)
    local inputs = require("neo-tree.ui.inputs")

    -- get table items count
    function GetTableLen(tbl)
        local len = 0
        for n in pairs(tbl) do
            len = len + 1
        end
        return len
    end

    local count = GetTableLen(selected_nodes)
    local msg = "Are you sure you want to trash " .. count .. " files ?"
    inputs.confirm(msg, function(confirmed)
        if not confirmed then return end
        for _, node in ipairs(selected_nodes) do
            vim.fn.system { "trash", vim.fn.fnameescape(node.path) }
        end
        require("neo-tree.sources.manager").refresh(state.name)
    end)
end

function getTelescopeOpts(state, path)
    return {
        cwd = path,
        search_dirs = { path },
        attach_mappings = function(prompt_bufnr, map)
            local actions = require "telescope.actions"
            actions.select_default:replace(function()
                actions.close(prompt_bufnr)
                local action_state = require "telescope.actions.state"
                local selection = action_state.get_selected_entry()
                local filename = selection.filename
                if (filename == nil) then
                    filename = selection[1]
                end
                -- any way to open the file without triggering auto-close event of neo-tree?
                require("neo-tree.sources.filesystem").navigate(state, state.path, filename)
            end)
            return true
        end
    }
end
