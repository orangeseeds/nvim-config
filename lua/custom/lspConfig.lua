local _border = "single"
local Border = {
    { "╭", "LspFloatWinBorder" },
    { "─", "LspFloatWinBorder" },
    { "╮", "LspFloatWinBorder" },
    { "│", "LspFloatWinBorder" },
    { "╯", "LspFloatWinBorder" },
    { "─", "LspFloatWinBorder" },
    { "╰", "LspFloatWinBorder" },
    { "│", "LspFloatWinBorder" },
}

local borderType = {
    "single",
    "double",
    "rounded",
    "solid",
    "shadow",
}

local border = Border

vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(
    vim.lsp.handlers.hover, {
        border = border,
        ctermbg = 220
    }
)

vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(
    vim.lsp.handlers.signature_help, {
        border = border
    }
)

vim.diagnostic.config {
    float = { border = border }
}

-- vim.cmd("highlight FloatBorder guifg=#ffffff guibg=none")
-- vim.cmd("highlight Pmenu guibg=none")
