--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system {
        'git',
        'clone',
        '--filter=blob:none',
        'https://github.com/folke/lazy.nvim.git',
        '--branch=stable', -- latest stable release
        lazypath,
    }
end
vim.opt.rtp:prepend(lazypath)


require("custom.helpers")
require("custom.lspConfig")
-- require("lspconfig").pyright.setup({})
require("custom.go")

require('lazy').setup({
    -- NOTE: First, some plugins that don't require any configuration
    'ThePrimeagen/git-worktree.nvim',

    'tpope/vim-fugitive',
    -- 'tpope/vim-rhubarb',

    {
        'neovim/nvim-lspconfig',
        dependencies = {
            -- Automatically install LSPs to stdpath for neovim
            { 'williamboman/mason.nvim', config = true },
            'williamboman/mason-lspconfig.nvim',


            -- Additional lua configuration, makes nvim stuff amazing!
            'folke/neodev.nvim',

            -- auto pairs tags
            "windwp/nvim-autopairs",
            "windwp/nvim-ts-autotag",
        },
    },
    {
        'j-hui/fidget.nvim',
        tag = 'legacy',
        -- config = function() require 'extensions.fidget' end,
        requires = 'neovim/nvim-lspconfig',
        opts = {}
    },

    {
        -- Autocompletion
        'hrsh7th/nvim-cmp',
        dependencies = {
            'hrsh7th/cmp-nvim-lsp',
            'L3MON4D3/LuaSnip',
            'saadparwaiz1/cmp_luasnip'
        },
    },

    -- Useful plugin to show you pending keybinds.
    -- { 'folke/which-key.nvim',          opts = {} },
    {
        -- Adds git releated signs to the gutter, as well as utilities for managing changes
        'lewis6991/gitsigns.nvim',
        opts = {
            -- See `:help gitsigns.txt`
            signs = {
                add = { text = '+' },
                change = { text = '~' },
                delete = { text = '_' },
                topdelete = { text = '‾' },
                changedelete = { text = '~' },
            },
        },
    },

    {
        -- Set lualine as statusline
        'nvim-lualine/lualine.nvim',
        -- See `:help lualine.txt`
        opts = {
            options = {
                icons_enabled = false,
                -- theme = 'gruvbox_dark',
                component_separators = '|',
                section_separators = '',
            },
        },
    },

    -- "gc" to comment visual regions/lines
    { 'numToStr/Comment.nvim',  opts = {} },

    -- Fuzzy Finder (files, lsp, etc)
    {
        'nvim-telescope/telescope.nvim',
        version = '*',
        dependencies = {
            'nvim-lua/plenary.nvim',
            "RRethy/nvim-treesitter-endwise",
            'JoosepAlviste/nvim-ts-context-commentstring',
        }
    },

    -- Fuzzy Finder Algorithm which requires local dependencies to be built.
    -- Only load if `make` is available. Make sure you have the system
    -- requirements installed.
    {
        'nvim-telescope/telescope-fzf-native.nvim',
        -- NOTE: If you are having trouble with this installation,
        --       refer to the README for telescope-fzf-native for more instructions.
        build = 'make',
        cond = function()
            return vim.fn.executable 'make' == 1
        end,
    },
    {
        -- Highlight, edit, and navigate code
        -- 'nvim-treesitter/nvim-treesitter',
        -- build = ":TSUpdate",
        --
        'nvim-treesitter/nvim-treesitter',
        dependencies = {
            'nvim-treesitter/nvim-treesitter-textobjects',
        },
        build = ':TSUpdate',
    },




    { import = 'custom.plugins' },
}, {})

-- [[ Setting options ]]
-- See `:help vim.o`

-- Set highlight on search
vim.o.hlsearch = false

-- Make line numbers default
vim.wo.number = true

vim.o.relativenumber = true
vim.o.sidescrolloff = 10
vim.o.wrap = false
-- vim.o.sidescroll = 5

-- Enable mouse mode
vim.o.mouse = 'a'

-- Sync clipboard between OS and Neovim.
--  Remove this option if you want your OS clipboard to remain independent.
--  See `:help 'clipboard'`
vim.o.clipboard = 'unnamedplus'

-- Enable break indent
-- vim.o.breakindent = true

-- Save undo history
vim.o.undofile = true

-- Case insensitive searching UNLESS /C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- Keep signcolumn on by default
vim.wo.signcolumn = 'yes'

-- Decrease update time
vim.o.updatetime = 1000
vim.o.timeout = true
vim.o.timeoutlen = 800

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

-- NOTE: You should make sure your terminal supports this
vim.o.termguicolors = true

vim.o.expandtab = true
vim.o.tabstop = 4
vim.o.shiftwidth = 4
vim.o.cursorline = true
vim.o.splitright = true
vim.o.splitbelow = true

-- vim.cmd('colorscheme railscasts2')
-- vim.cmd('colorscheme mustang')
-- vim.cmd('set colorcolumn=100')
vim.cmd('set scrolloff=10')

-- [[ Basic Keymaps ]]

-- Keymaps for better default experience
-- See `:help vim.keymap.set()`

-- Remap for dealing with word wrap
-- vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
-- vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- [[ Configure Telescope ]]
-- See `:help telescope` and `:help telescope.setup()`
require('telescope').setup {
    pickers = {
        colorscheme = {
            enable_preview = true
        }
    },
    defaults = {
        -- file_ignore_patterns ={
        --     "vendor"
        -- },
        mappings = {
            i = {
                -- ['<C-u>'] = false,
                -- ['<C-d>'] = false,
            },
        },
    },
}


require('git-worktree').setup()

-- Enable telescope fzf native, if installed
pcall(require('telescope').load_extension, 'fzf')
require("telescope").load_extension("git_worktree")

-- See `:help telescope.builtin`

---@diagnostic disable-next-line: missing-fields
require('nvim-treesitter.configs').setup {
    -- Add languages to be installed here that you want installed for treesitter
    ensure_installed = { 'c', 'go', 'cpp', 'lua', 'python', 'rust', 'tsx', 'typescript', 'vimdoc', 'vim', 'markdown', 'vue', 'php' },

    -- Autoinstall languages that are not installed. Defaults to false (but you can change for yourself!)
    auto_install = false,
    -- ignore_install = {},
    -- sync_install = false,
    -- modules = {},

    highlight = { enable = true },
    indent = { enable = true, disable = { 'python' } },
    -- incremental_selection = {
    --     enable = false,
    --     keymaps = {
    --         init_selection = '<c-space>',
    --         -- node_incremental = '<c-space>',
    --         scope_incremental = '<c-s>',
    --         node_decremental = '<M-space>',
    --     },
    -- },
    textobjects = {
        select = {
            enable = true,
            disable = { "zig" },
            lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
            keymaps = {
                -- You can use the capture groups defined in textobjects.scm
                -- This is used in select mode, eg 'vaf' -> select function outer.
                -- ['aa'] = '@parameter.outer',
                -- ['ia'] = '@parameter.inner',
                ['af'] = '@function.outer',
                ['if'] = '@function.inner',
                ['ac'] = '@class.outer',
                ['ic'] = '@class.inner',
            },
        },
        -- NOTE: 'enable' has been set to false!!
        --
        -- move = {
        --   enable = false,
        --   set_jumps = false, -- whether to set jumps in the jumplist
        --   goto_next_start = {
        --     [']m'] = '@function.outer',
        --     [']]'] = '@class.outer',
        --   },
        --   goto_next_end = {
        --     [']M'] = '@function.outer',
        --     [']['] = '@class.outer',
        --   },
        --   goto_previous_start = {
        --     ['[m'] = '@function.outer',
        --     ['[['] = '@class.outer',
        --   },
        --   goto_previous_end = {
        --     ['[M'] = '@function.outer',
        --     ['[]'] = '@class.outer',
        --   },
        -- },
        -- swap = {
        --   enable = false,
        --   swap_next = {
        --     ['<leader>a'] = '@parameter.inner',
        --   },
        --   swap_previous = {
        --     ['<leader>A'] = '@parameter.inner',
        --   },
        -- },
    },
}

-- Enable the following language servers
--  Feel free to add/remove any LSPs that you want here. They will automatically be installed.
--
--  Add any additional override configuration in the following tables. They will be passed to
--  the `settings` field of the server config. You must look up that documentation yourself.
--
local util = require("lspconfig.util")
local servers = {
    -- clangd = {},
    -- gopls = {},
    -- pyright = {},
    -- rust_analyzer = {},
    -- tsserver = {},

    lua_ls = {
        Lua = {
            workspace = { checkThirdParty = false },
            telemetry = { enable = false },
            -- hint = { enable = true },
        },
    },
}


-- Setup neovim lua configuration
require('neodev').setup()

-- require 'lspconfig'.biome.setup { }
-- nvim-cmp supports additional completion capabilities, so broadcast that to servers
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

-- Ensure the servers above are installed
local mason_lspconfig = require 'mason-lspconfig'

mason_lspconfig.setup {
    ensure_installed = vim.tbl_keys(servers),
}

mason_lspconfig.setup_handlers {
    function(server_name)
        require('lspconfig')[server_name].setup {
            capabilities = capabilities,
            settings = servers[server_name],
        }
    end,
}
require("nvim-autopairs").setup({ check_ts = true })
require("nvim-ts-autotag").setup({ enable = true })

-- nvim-cmp setup
local cmp = require 'cmp'
local luasnip = require 'luasnip'

luasnip.config.setup {}

---@diagnostic disable-next-line: missing-fields
cmp.setup {
    -- revision, enabled, performance, preselect, completion, confirmation, matching, sorting, formatting, view, experimental,
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end,
    },
    mapping = cmp.mapping.preset.insert {
        ['<C-n>'] = cmp.mapping.select_next_item(),
        ['<C-p>'] = cmp.mapping.select_prev_item(),
        ['<C-d>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-Space>'] = cmp.mapping.complete {},
        ['<CR>'] = cmp.mapping.confirm {
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
        },
        ['<Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            elseif luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            else
                fallback()
            end
        end, { 'i', 's' }),
        ['<S-Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
                luasnip.jump(-1)
            else
                fallback()
            end
        end, { 'i', 's' }),
    },
    sources = {
        { name = 'nvim_lsp' },
        { name = 'luasnip' },
    },
}

require("neo-tree").setup({
    event_handlers = {

        {
            event = "file_opened",
            handler = function(file_path)
                -- auto close
                -- vimc.cmd("Neotree close")
                -- OR
                require("neo-tree.command").execute({ action = "close" })
            end
        },

    },
    filesystem = {
        window = {
            position = "current",
            mappings = {
                ["/"] = "",
                ["<c-x>"] = "",
                ["tf"] = "telescope_find",
                ["tg"] = "telescope_grep",
                ["v"] = "clear_filter",
                ["F"] = "fuzzy_finder",
            },
        },
        commands = {
            delete = function(state)
                trash(state)
            end,
            delete_visual = function(state, selected_nodes)
                trash_visual(state, selected_nodes)
            end,
            telescope_find = function(state)
                local node = state.tree:get_node()
                local path = node:get_id()
                require('telescope.builtin').find_files(getTelescopeOpts(state, path))
            end,
            telescope_grep = function(state)
                local node = state.tree:get_node()
                local path = node:get_id()
                require('telescope.builtin').live_grep(getTelescopeOpts(state, path))
            end,
        },

    },
})


-- Settingup lualine
--
require("lualine").setup {
    globalstatus = true,
    options = {
        icons_enabled = true,
        disabled_filetypes = {
            statusline = {},
            winbar = {},
        },
        ignore_focus = {},
        always_divide_middle = true,
        globalstatus = false,
        refresh = {
            statusline = 1000,
            tabline = 1000,
            winbar = 1000,
        }
    },
    sections = {
        lualine_a = { 'mode' },
        lualine_b = { 'branch', 'diff', 'diagnostics' },
        lualine_c = { function()
            local root = vim.fn.getcwd() -- Start with the current working directory
            local git_dir = vim.fn.finddir('.git', root .. ';')
            if git_dir ~= '' then
                root = vim.fn.fnamemodify(git_dir, ':h:h') -- Go up two levels to get the project root
            end
            local current_file = vim.fn.expand('%:p')
            local relative = vim.fn.fnamemodify(current_file, ':~:.')
            return relative
        end },
        lualine_x = { 'encoding',
            { 'filetype', icons_enabled = false }
        },
        lualine_y = { 'progress' },
        lualine_z = { 'location' }
    },
    inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = { 'filename' },
        lualine_x = { 'location' },
        lualine_y = {},
        lualine_z = {}
    },
    tabline = {},
    winbar = {},
    inactive_winbar = {},
    extensions = {}
}

vim.filetype.add({
    extension = {
        mdx = 'mdx'
    }
})
vim.treesitter.language.register("markdown", "mdx")
-- vim.cmd('source ~/.config/nvim/cursor.vim')
vim.cmd('source ~/.config/nvim/busybee.vim')
--
--
--
require("telescope").setup({
    defaults = {
        mappings = {
            i = {
                ['<C-p>'] = require('telescope.actions.layout').toggle_preview
            }
        },
        preview = {
            hide_on_startup = true -- hide previewer when picker starts
        }
    }
})

require("nvim-web-devicons").setup { default = true }
